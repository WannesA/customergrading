page 50130 "SOL Grade Card"
{
    PageType = Card;
    Caption = 'Grade Card';
    SourceTable = "SOL Grade";


    layout
    {

        area(Content)
        {
            group(General)
            {
                field("No."; rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Grade No.';

                    trigger OnAssistEdit()
                    begin
                        if Rec.AssistEdit() then
                            CurrPage.Update();
                    end;

                }
                field("Description"; rec.Description)
                {
                    ApplicationArea = all;
                    ToolTip = 'Grade Description';
                }

                field("No. series"; rec."No.series")
                {
                    ApplicationArea = all;
                    ToolTip = 'Grade No. Series';
                }
            }
        }
        area(FactBoxes)
        {

            systempart(Recordlinks; Links)
            {
                ApplicationArea = all;
            }
            systempart(Notes; Notes)
            {
                ApplicationArea = all;
            }


        }
    }

}