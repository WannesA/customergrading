page 50132 "SOL Grade List"
{
    PageType = List;
    Caption = 'Grade List';
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "SOL Grade";
    CardPageId = "SOL Grade Card";
    Editable = false;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field("No."; rec."No.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Grade No.';

                }
                field(Description; rec.Description)
                {
                    ApplicationArea = all;
                    ToolTip = 'Grade Description';
                }
            }
        }

        area(FactBoxes)
        {

            systempart(Recordlinks; Links)
            {
                ApplicationArea = all;
            }
            systempart(Notes; Notes)
            {
                ApplicationArea = all;
            }


        }
    }

}