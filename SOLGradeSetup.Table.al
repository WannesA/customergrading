table 50131 "SOL Grade Setup"
{
    DataClassification = SystemMetadata;
    Caption = 'Grade Setup';
    //ted-s

    fields
    {

        field(1; "Primary key"; Code[10])
        {
            DataClassification = CustomerContent;
            Caption = 'Primary key';

        }
        field(2; "Grade Nos."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Grade Nos.';
            TableRelation = "No. Series";

        }
    }

    keys
    {
        key(PK; "Primary key")
        {
            Clustered = true;
        }
    }

    var
        myInt: Integer;

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}