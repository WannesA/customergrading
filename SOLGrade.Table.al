table 50130 "SOL Grade"
{
    DataClassification = CustomerContent;
    Caption = 'Grade';
    LookupPageId = "SOL Grade List";
    DrillDownPageId = "SOL Grade List";


    fields
    {

        field(1; "No."; Code[20])
        {
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                GradeSetup: Record "SOL Grade Setup";
                NoSeriesMngt: Codeunit NoSeriesManagement;
            begin
                if "No." = xRec."No." then
                    exit;

                GradeSetup.Get();
                NoSeriesMngt.TestManual(GradeSetup."Grade Nos.");
                "No.series" := '';
            end;

        }
        field(2; "Description"; Text[100])
        {
            DataClassification = CustomerContent;
        }
        field(3; "No.series"; Code[20])
        {
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "No. Series";
            Caption = 'No. Series';
        }

    }

    keys
    {
        key(PK; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
        fieldgroup(DropDown; "No.", Description) { }
        fieldgroup(Brick; "No.", Description) { }

    }

    trigger OnInsert()
    var
        GradeSetup: Record "SOL Grade Setup";
        NoSeriesMngt: Codeunit NoSeriesManagement;

    begin
        if Rec."No." = '' then begin
            GradeSetup.Get();
            GradeSetup.TestField("Grade Nos.");
            NoSeriesMngt.InitSeries(GradeSetup."Grade Nos.", xRec."No.series", 0D, "No.", "No.series");
        end;
    end;

    procedure AssistEdit(): Boolean
    var
        Grade: Record "SOL Grade";
        GradeSetup: Record "SOL Grade Setup";
        NoSeriesMngt: Codeunit NoSeriesManagement;
    begin
        Grade := Rec;
        GradeSetup.Get();
        GradeSetup.TestField("Grade Nos.");
        if NoSeriesMngt.SelectSeries(GradeSetup."Grade Nos.", xRec."No.", Grade."No.series") then begin
            NoSeriesMngt.SetSeries(Grade."No.");
            Rec := Grade;
        end;
    end;


    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;


}