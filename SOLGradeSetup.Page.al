page 50131 "SOL Grade Setup"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "SOL Grade Setup";
    Caption = 'Grade Setup';
    InsertAllowed = false;
    DeleteAllowed = false;

    layout
    {

        area(Content)
        {
            group(Numbering)
            {
                Caption = 'Numbering';
                field("Grade Nos."; Rec."Grade Nos.")
                {
                    ApplicationArea = All;
                    ToolTip = 'Grade nos';

                }
            }
        }
    }
    trigger OnOpenPage()
    begin
        if not rec.Get() then
            rec.Insert();
    end;
}